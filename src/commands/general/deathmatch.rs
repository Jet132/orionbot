use serenity::{

    framework::standard::{
        CommandResult, Args,
        macros::command
    },

    client::bridge::gateway::{ ShardMessenger },

    http::client::Http,
};

use rand::prelude::*;

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use chrono::prelude::*;

use std::sync::Arc;

use crate::cache::*;
use nebbot_utils::*;

#[command]
#[only_in(guilds)]
#[description("Fight another user to the death!")]
#[usage("!deathmatch <user>")]
async fn deathmatch(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;

    let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let random = rand::rngs::OsRng;

    let discord_id = *msg.author.id.as_u64();

    let user = match match_discord_user(&args, ctx.cache.users().await, discord_id) {
        MatchDiscordUser::Other(u) => u,
        MatchDiscordUser::Bot(_) => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Deathmatch")
                    .description("You cannot fight a bot!")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        },
        MatchDiscordUser::SameAsHost => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Deathmatch")
                    .description("You cannot fight yourself!")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        },
        MatchDiscordUser::NoUserFound |
        MatchDiscordUser::NoArgument |
        MatchDiscordUser::InvalidForm(_) => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Deathmatch")
                    .description("Invalid User Argument")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        }
    };

    if bot_cache.lock().await.deathmatch.contains(&discord_id) {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Deathmatch")
                .description("You are already in a deathmatch!")
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;

        return Ok(());
    }

    if bot_cache.lock().await.deathmatch.contains(&user.id.as_u64()) {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Deathmatch")
                .description("Your opponent is already in a deathmatch!")
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;

        return Ok(());
    }

    let mut confirmation = msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Deathmatch")
            .description(format!("{}, react to confirm deathmatch!", user.mention()))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await?;

    confirmation.react(&ctx.http, ReactionType::Unicode("✅".to_string())).await?;
    confirmation.react(&ctx.http, ReactionType::Unicode("❌".to_string())).await?;

    let mut start_time = Utc::now();
    let mut timeout = std::time::Duration::from_secs(30);

    loop {
        match confirmation.await_reaction(&ctx).timeout(timeout).author_id(user.id).await {
            Some(reaction) => {
                let emoji = &reaction.as_inner_ref().emoji;

                match emoji.as_data().as_str() {
                    "✅" => {
                        break;
                    }
                    "❌" => {
                        confirmation.edit(&ctx.http, |m| {
                            m
                            .embed(|e| {
                                e
                                .title("Deathmatch")
                                .description("~~React to confirm deathmatch!~~\n\nCancelled.")
                                .colour(Colour::from_rgb(52, 152, 219))
                            })
                        }).await?;

                        return Ok(());
                    }

                    _ => {
                        let now = Utc::now();
                        timeout -= (now - start_time).to_std().unwrap();
                        start_time = now;
                    }
                }
            }

            None => {
                confirmation.edit(&ctx.http, |m| {
                    m
                    .embed(|e| {
                        e
                        .title("Deathmatch")
                        .description("~~React to confirm deathmatch!~~\n\nTime out.")
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;

                return Ok(());
            }
        }
    }

    {
        let mut lock = bot_cache.lock().await;
        lock.deathmatch.push(discord_id);
        lock.deathmatch.push(*user.id.as_u64());
    }

    let host: Arc<Mutex<Vec<i32>>> = Arc::new(Mutex::new(Vec::new()));
    let other: Arc<Mutex<Vec<i32>>> = Arc::new(Mutex::new(Vec::new()));
    // Host
    let host_future = play(ctx.shard.clone(), ctx.http.clone(), random, host.clone(), UserId(discord_id));

    // Other
    let other_future = play(ctx.shard.clone(), ctx.http.clone(), random, other.clone(), user.id);

    // Wait until complete
    futures::future::join(host_future, other_future).await;
    
    let mut host_health: i32 = 100;
    let mut other_health: i32 = 100;

    let mut desc = format!("{} vs {}!\n\n```", msg.author.mention(), user.mention());

    let _host_damage_lock = host.lock().await;
    let _other_damage_lock = other.lock().await;

    let mut host_damage = _host_damage_lock.iter();
    let mut other_damage = _other_damage_lock.iter();

    for round in 1i32..11 {
        let this_host_damage = other_damage.next().unwrap_or(&0);
        let this_other_damage = host_damage.next().unwrap_or(&0);

        host_health -= this_host_damage;
        other_health -= this_other_damage;

        desc.push_str(format!("{}. [{}] -{} || -{} [{}]\n", round, host_health, this_host_damage, this_other_damage, other_health).as_str());

        if host_health <= 0 && other_health <= 0 {
            desc.push_str("```\nIt's a draw!");
            break;
        }

        else if host_health <= 0 {
            desc.push_str(format!("```\n{} Wins!", user.mention()).as_str());
            break;
        }

        else if other_health <= 0 {
            desc.push_str(format!("```\n{} Wins!", msg.author.mention()).as_str());
            break;
        }
    }

    if host_health > 0 && other_health > 0 {
        if host_health == other_health {
            desc.push_str("```\nIt's a draw!");
        }
        else if host_health >= other_health {
            desc.push_str(format!("```\nIt's a draw, but {} did more damage!", msg.author.mention()).as_str());
        }
        else if host_health <= other_health {
            desc.push_str(format!("```\nIt's a draw, but {} did more damage!", user.mention()).as_str());
        }
        
    }

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Deathmatch")
            .description(desc)
        })
    }).await?;

    {
        let mut lock = bot_cache.lock().await;

        let index_host = lock.deathmatch.iter().position(|&x| x == discord_id).expect("Could not find discord id in deathmatch");
        lock.deathmatch.remove(index_host);

        let index_other = lock.deathmatch.iter().position(|&x| x == *user.id.as_u64()).expect("Could not find discord id in deathmatch");
        lock.deathmatch.remove(index_other);
    }

    Ok(())
}

async fn play(shard_messenger: ShardMessenger, http: Arc<Http>, mut random: rand::rngs::OsRng, damage_total: Arc<Mutex<Vec<i32>>>, user: UserId) {
    let channel = match user.create_dm_channel(http.clone()).await {
        Ok(c) => c,
        Err(e) => {
            error!("Deathmatch -- Could not create dm with {} -- {}", user.0, e);
            return;
        }
    };
    
    let mut damage: i32 = random.gen_range(10, 20);

    let mut message = match channel.send_message(http.clone(), |m| {
        m.embed(|e| {
            e
            .title("Deathmatch")
            .description(format!("Deal {} damage or re-roll to have a chance at higher damages?", damage))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await {
        Ok(m) => m,
        Err(e) => {
            error!("Deathmatch -- Error sending message {}", e);
            return;
        }
    };

    match message.react(http.clone(), ReactionType::Unicode("🗡️".to_string())).await {
        Ok(_) => {},
        Err(e) => {
            error!("Deathmatch -- Error reacting {}", e);
            return;
        }
    };

    match message.react(http.clone(), ReactionType::Unicode("🎲".to_string())).await {
        Ok(_) => {},
        Err(e) => {
            error!("Deathmatch -- Error reacting {}", e);
            return;
        }
    };
    
    let mut reroll_amount: i32 = 0;
    
    let mut start_time = Utc::now();
    let mut timeout = std::time::Duration::from_secs(30);

    for round in 1i32..11 {
        loop {
            match message.await_reaction(&shard_messenger).timeout(timeout).author_id(user).await {
                Some(reaction) => {
                    let reaction = reaction.as_inner_ref();
                    let emoji = &reaction.emoji;

                    match emoji.as_data().as_str() {
                        "🎲" => {

                            reroll_amount += 1;

                            damage_total.lock().await.push(0);

                            match message.delete(http.clone()).await{
                                Ok(_) => {},
                                Err(e) => {
                                    error!("Deathmatch -- Error removing message {}", e);
                                    return;
                                }
                            };

                            break;
                        }
                        "🗡️" => {

                            damage_total.lock().await.push(damage);

                            match message.delete(http.clone()).await{
                                Ok(_) => {},
                                Err(e) => {
                                    error!("Deathmatch -- Error removing message {}", e);
                                    return;
                                }
                            };

                            break;
                        }

                        _ => {
                            let now = Utc::now();
                            let next = (now - start_time).to_std().unwrap();

                            if next >= timeout {
                                match message.edit(http.clone(), |m| {
                                    m
                                    .embed(|e| {
                                        e
                                        .title("Deathmatch")
                                        .description(format!("~~Deal {} damage or re-roll to have a chance at higher damages?~~\n\nTime out!", damage))
                                        .colour(Colour::from_rgb(52, 152, 219))
                                    })
                                }).await{
                                    Ok(_) => {},
                                    Err(e) => {
                                        error!("Deathmatch -- Error editting message {}", e);
                                        return;
                                    }
                                };

                                return;
                            }

                            timeout -= next;
                            start_time = now;
                        }
                    }
                }

                None => {
                    match message.edit(http.clone(), |m| {
                        m
                        .embed(|e| {
                            e
                            .title("Deathmatch")
                            .description(format!("~~Deal {} damage or re-roll to have a chance at higher damages?~~\n\nTime out!", damage))
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await{
                        Ok(_) => {},
                        Err(e) => {
                            error!("Deathmatch -- Error editting message {}", e);
                            return;
                        }
                    };

                    return;
                }
            }
        }

        damage = random.gen_range(10.0, 20.0 * (1.5 * (reroll_amount as f32 + 2.0).ln()) + 15.0 * (round as f32 + 1.0).ln()).round() as i32;

        if round <= 9 {
            message = match channel.send_message(http.clone(), |m| {
                m
                .embed(|e| {
                    e
                    .title("Deathmatch")
                    .description(format!("Deal {} damage or re-roll to have a chance at higher damages?", damage))
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await {
                Ok(m) => m,
                Err(e) => {
                    error!("Deathmatch -- Error editting message {}", e);
                    return;
                }
            };
    
            match message.react(http.clone(), ReactionType::Unicode("🗡️".to_string())).await {
                Ok(_) => {},
                Err(e) => {
                    error!("Deathmatch -- Error reacting {}", e);
                    return;
                }
            };
    
            match message.react(http.clone(), ReactionType::Unicode("🎲".to_string())).await {
                Ok(_) => {},
                Err(e) => {
                    error!("Deathmatch -- Error reacting {}", e);
                    return;
                }
            };
    
            let now = Utc::now();
            let next = (now - start_time).to_std().unwrap();

            if next >= timeout {
                match message.edit(http.clone(), |m| {
                    m
                    .embed(|e| {
                        e
                        .title("Deathmatch")
                        .description(format!("~~Deal {} damage or re-roll to have a chance at higher damages?~~\n\nTime out!", damage))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await{
                    Ok(_) => {},
                    Err(e) => {
                        error!("Deathmatch -- Error editting message {}", e);
                        return;
                    }
                };

                return;
            }

            timeout -= next;
            start_time = now;
        }
    }
}