use serenity::{

    framework::standard::{
        CommandResult, macros::command
    },

    collector::MessageCollectorBuilder,
    futures::stream::StreamExt,
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use chrono::prelude::*;
use chrono::Duration;

use crate::cache::*;
use crate::db::*;
use nebbot_utils::*;

use diesel::SaveChangesDsl;

#[command]
#[aliases("spinner")]
#[description("Spin!")]
#[only_in(guilds)]
async fn spin(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");
    let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();
    
    {
        match database.get_user(discord_id, guild_id).await? {
            Some(mut user) => {
                let duration = Utc::now() - user.last_spin;

                if duration <= Duration::minutes(super::SPINNER_COOLDOWN) {

                    let time_next = Duration::minutes(super::SPINNER_COOLDOWN) - duration;

                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Spin")
                            .description(format!("You need to wait {}",
                                human_readable(time_next)))
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;

                    return Ok(());
                }

                user.last_spin = Utc::now();

                let _: DbUser = user.save_changes(&*database.conn.lock().await)?;
            }

            None => {
                let mut default = DbUser::default(discord_id, guild_id);

                default.last_spin = Utc::now();

                database.add_user(default).await?;
            }
        }
    }

    if bot_cache.lock().await.spinning.contains(&discord_id) {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Spinner")
                .description("You are already spinning!")
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;

        return Ok(());
    }

    let start_time = Utc::now();

    {
        let mut lock = bot_cache.lock().await;
        lock.spinning.push(discord_id);
    }

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Spinner")
            .description(format!("{}, your spinner is spinning! Do not allow for other users to message here, or they will knock it over!",
                msg.author.mention()))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await?;

    let mut collector = MessageCollectorBuilder::new(&ctx)
        .channel_id(msg.channel_id)
        .await;

    loop {
        let message = match collector.next().await {
            Some(m) => m,
            None => {
                error!("Spin collector none.");
                break;
            }
        };

        if message.author.bot { continue; }

        if message.author.id == msg.author.id {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Spinner")
                    .description(format!("{}, do not try to hide your message!",
                        message.author.mention()))
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            {
                let mut lock = bot_cache.lock().await;
                let index = lock.spinning.iter()
                    .position(|&x| x == discord_id).expect("Could not find discord id in spinning");
                lock.spinning.remove(index);
            }

            return Ok(());
        }

        let duration = Utc::now() - start_time;

        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Spinner")
                .description(format!("Uh oh! {} knocked over {}'s spinner! It lasted {}!",
                    message.author.mention(), msg.author.mention(), human_readable(duration)))
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;
  
        match database.get_spinner(discord_id, guild_id).await? {
            Some(mut spinner) => {
                if duration.num_seconds() >= spinner.duration {
                    spinner.channel_id = TextU64(*msg.channel_id.as_u64());
                    spinner.knocked_user_id = TextU64(*message.author.id.as_u64());
                    spinner.duration = duration.num_seconds();

                    let _: DbSpinner = spinner.save_changes(&*database.conn.lock().await)?;
                }
            }

            None => {
                let new = DbSpinner {
                    discord_id: TextU64(discord_id),
                    guild_id: TextU64(guild_id),
                    channel_id: TextU64(*msg.channel_id.as_u64()),
                    knocked_user_id: TextU64(*message.author.id.as_u64()),
                    duration: duration.num_seconds()
                };
                
                database.add_spinner(new).await?;
            }
        }

        break;
    }

    let mut lock = bot_cache.lock().await;
    let index = lock.spinning.iter()
        .position(|&x| x == discord_id).expect("Could not find discord id in spinning");
    lock.spinning.remove(index);

    Ok(())
}