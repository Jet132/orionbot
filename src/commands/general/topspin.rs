use serenity::framework::standard::{
    CommandResult, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use chrono::Duration;

use nebbot_utils::*;

#[command]
#[only_in(guilds)]
#[description("View spinner leaderboard!")]
async fn topspin(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let spinners = database.select_top_10_spinners(*msg.guild_id.unwrap().as_u64()).await?;

    let mut fields = Vec::new();

    for (i, spinner) in spinners.iter().enumerate() {

        let cached_users = ctx.cache.users().await;

        let user = match cached_users.get(&UserId(spinner.discord_id.0)) {
            Some(u) => u,
            None => { continue; }
        };

        fields.push((format!("{}. {}", i+1, user.tag()), format!("Duration: {}\nChannel: <#{}>\nKnocked by: <@{}>\n",
        human_readable(Duration::seconds(spinner.duration)),
        spinner.channel_id.0, spinner.knocked_user_id.0)));
    }

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Spinner Leaderboard")
            .description("Top 10 spinners")
            .colour(Colour::from_rgb(52, 152, 219));

            for field in fields {
                e.field(field.0, field.1, false);
            }

            e
        })
    }).await?;

    Ok(())
}