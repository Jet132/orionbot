use serenity::framework::standard::{
    CommandResult, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use chrono::prelude::*;
use chrono::Duration;

use crate::db::*;
use nebbot_utils::*;

#[command]
#[only_in(guilds)]
#[aliases("timeouts","timeout","cooldowns")]
#[description("Lists timeout for certain commands!")]
async fn cooldown(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let user = match database.get_user(discord_id, guild_id).await? {
        Some(u) => u,
        None => {
            let default = DbUser::default(discord_id, guild_id);
            database.add_user(default.clone()).await?;

            default
        }
    };

    let now = Utc::now();

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Cooldown")
            .description("Commands with cooldown: ")
            .colour(Colour::from_rgb(52, 152, 219));

            {
                let message = if user.arrested {
                    if now - user.last_bankrob <= Duration::hours(super::super::ARRESTED_COOLDOWN) {
                        human_readable(Duration::hours(super::super::ARRESTED_COOLDOWN) - (now - user.last_bankrob))
                    }
                    else { "Available".to_owned() }
                }
                else {
                    if now - user.last_hourly >= Duration::hours(1) { "Available".to_owned() } 
                    else { human_readable(Duration::hours(1) - (now - user.last_hourly)) } 
                };
                
                e.field("Hourly", message, false);
            }
            
            {
                let message = if user.arrested {
                    if now - user.last_bankrob <= Duration::hours(super::super::ARRESTED_COOLDOWN) {
                        human_readable(Duration::hours(super::super::ARRESTED_COOLDOWN) - (now - user.last_bankrob))
                    }
                    else { "Available".to_owned() }
                }
                else {
                    if now - user.last_mine >= Duration::hours(super::super::economy::MINE_COOLDOWN) { "Available".to_owned() } 
                    else { human_readable(Duration::hours(super::super::MINE_COOLDOWN) - (now - user.last_mine)) }
                };

                e.field("Mine", message, false);
            }

            {
                let message = if user.arrested {
                    if now - user.last_bankrob <= Duration::hours(super::super::ARRESTED_COOLDOWN) {
                        human_readable(Duration::hours(super::super::ARRESTED_COOLDOWN) - (now - user.last_bankrob))
                    }
                    else { "Available".to_owned() }
                }
                else {
                    if now - user.last_spin >= Duration::minutes(super::SPINNER_COOLDOWN) { "Available".to_owned() } 
                    else { human_readable(Duration::minutes(super::SPINNER_COOLDOWN) - (now - user.last_spin)) }
                };

                e.field("Spin", message, false);
            }

            {

                let duration = now - user.last_bankrob;

                let message = if user.arrested {
                    if duration <= Duration::hours(super::super::ARRESTED_COOLDOWN) {
                        human_readable(Duration::hours(super::super::ARRESTED_COOLDOWN) - duration)
                    }
                    else { "Available".to_owned() }
                }
                else {
                    if duration <= Duration::hours(super::super::BANKROB_COOLDOWN) {
                        human_readable(Duration::hours(super::super::BANKROB_COOLDOWN) - duration)
                    }
                    else { "Available".to_owned() }
                };

                e.field("Bank Rob", message, false);
            }

            e
        })
    }).await?;

    Ok(())
}
