use serenity::{

    framework::standard::{
        CommandResult, Args,
        macros::{
            command,
            group
        }
    },
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use crate::db::*;
use nebbot_utils::*;

#[group]
#[only_in(guilds)]
#[required_permissions("ADMINISTRATOR")]
#[commands(config)]
struct Config;

#[command]
#[only_in(guilds)]
#[required_permissions("ADMINISTRATOR")]
async fn config(ctx: &Context, msg: &Message, args: Args) -> CommandResult {

    let data = ctx.data.read().await;
    
    if let Some(s) = args.current() {

        let specific = s.to_lowercase();

        let bot_info = data.get::<crate::BotInfoContainer>().expect("Couldn't retrieve BotInfoContainer");

        let command_name = {
            let lock = bot_info.lock().await;

            match lock.command_list.get_key_value(&specific) {
                Some(c) => c.0.clone(),
                None => {
                    match lock.command_list.iter().find_map(|(key, val)| if val.contains(&specific.to_owned()) { Some(key) } else { None }) {
                        Some(c) => c.clone(),
                        None => {
                            msg.channel_id.send_message(&ctx.http, |m| {
                                m.embed(|e| {
                                    e
                                    .title("Configuration")
                                    .description("Invalid argument")
                                    .colour(Colour::from_rgb(52, 152, 219))
                                })
                            }).await?;

                            return Ok(());
                        }
                    }
                }
            }
        };

        return specific_config(ctx, msg, command_name.as_str()).await;
    }
    
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let guild_id = *msg.guild_id.unwrap().as_u64();

    let config = database.get_whitelists(guild_id).await?;

    let mut description = String::new();

    for (k, v) in config {

        if let Whitelist::Disabled = v.list_type {
            description.push_str(format!("`{}` List Disabled", k).as_str());
            continue;
        }

        let list = if v.list.is_empty() { "Empty".to_owned() } else {
            let mut channel_list = String::new();

            for id in v.list {
                channel_list.push_str(format!("<#{}> ", id).as_str());
            }

            channel_list
        };

        description.push_str(format!("`{}` {}: {}\n\n", k, v.list_type, list).as_str());
    }

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Configuration")
            .description(description)
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await?;

    Ok(())
}

async fn specific_config(ctx: &Context, msg: &Message, command_name: &str) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let guild_id = *msg.guild_id.unwrap().as_u64();

    let server_config = database.get_whitelists(guild_id).await?;

    let mut is_first = false;

    let config = match server_config.get(command_name) {
        Some(c) => c.clone(),
        None => {
            is_first = true;

            Configuration {
                list_type: Whitelist::Disabled,
                list: Vec::new()
            }
        }
    };

    let title = format!("`{}` Configuration", command_name);

    let mut description = String::new();

    match config.list_type {
        Whitelist::Whitelist => {
            description.push_str("Whitelist: ");

            for l in config.list.clone() {
                description.push_str(format!("<#{}> ", l).as_str());
            }
        
            description.push_str("\n\n
                React with ❌ to disable the whitelist\n
                React with ⚫ to convert to blacklist\n
                React with ➕ to add to the whitelist\n
                React with ➖ to remove from the whitelist\n
                React with ♻️ to clear whitelist\n
            ");
        },
        Whitelist::Blacklist => {
            description.push_str("Blacklist: ");

            for l in config.list.clone() {
                description.push_str(format!("<#{}>", l).as_str());
            }
        
            description.push_str("\n\n
                React with ❌ to disable the blacklist\n
                React with ⚪ to convert to whitelist\n
                React with ➕ to add to the blacklist\n
                React with ➖ to remove from the blacklist\n
                React with ♻️ to clear blacklist\n
            ");
        },
        Whitelist::Disabled => {
            description.push_str("\n\n
                React with ⚪ to enable whitelist\n
                React with ⚫ to enable blacklist\n
            ");
        },
    }

    let message = msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title(title.clone())
            .description(description)
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await?;

    match config.list_type {
        Whitelist::Whitelist => {
            message.react(&ctx.http, ReactionType::Unicode("❌".to_string())).await?;
            message.react(&ctx.http, ReactionType::Unicode("⚫".to_string())).await?;
            message.react(&ctx.http, ReactionType::Unicode("➕".to_string())).await?;
            message.react(&ctx.http, ReactionType::Unicode("➖".to_string())).await?;
            message.react(&ctx.http, ReactionType::Unicode("♻️".to_string())).await?;
        },
        Whitelist::Blacklist => {
            message.react(&ctx.http, ReactionType::Unicode("❌".to_string())).await?;
            message.react(&ctx.http, ReactionType::Unicode("⚪".to_string())).await?;
            message.react(&ctx.http, ReactionType::Unicode("➕".to_string())).await?;
            message.react(&ctx.http, ReactionType::Unicode("➖".to_string())).await?;
            message.react(&ctx.http, ReactionType::Unicode("♻️".to_string())).await?;
        },
        _ => {
            message.react(&ctx.http, ReactionType::Unicode("⚪".to_string())).await?;
            message.react(&ctx.http, ReactionType::Unicode("⚫".to_string())).await?;
        },
    }

    match message.await_reaction(&ctx).timeout(std::time::Duration::from_secs(60)).author_id(msg.author.id).await {
        Some(reaction) => {
            let emoji = &reaction.as_inner_ref().emoji;

            match emoji.as_data().as_str() {
                "♻️" => {
                    database.clear_config(guild_id, command_name).await?;

                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title(title)
                            .description("Successfully cleared list!")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
                }
                "❌" => {
                    database.update_config(guild_id, command_name, Whitelist::Disabled).await?;

                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title(title)
                            .description("Successfully disabled list!")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
                }
                "⚪" => {
                    if is_first {
                        database.add_command_config(guild_id, command_name, Whitelist::Whitelist).await?;
                    }
                    else
                    {
                        database.update_config(guild_id, command_name, Whitelist::Whitelist).await?;
                    }

                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title(title)
                            .description("Successfully enabled whitelist!")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
                }
                "⚫" => {
                    if is_first {
                        database.add_command_config(guild_id, command_name, Whitelist::Blacklist).await?;
                    }
                    else {
                        database.update_config(guild_id, command_name, Whitelist::Blacklist).await?;
                    }

                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title(title)
                            .description("Successfully enabled blacklist!")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
                }
                "➕" => {
                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title(title.clone())
                            .description("Enter a list of channels to add to the list")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;

                    let mut to_add = match msg.author.await_reply(&ctx).timeout(std::time::Duration::from_secs(60)).await {
                        Some(m) => {
                            parse_channels(m.content.clone())
                        }
                        None => {
                            return Ok(());
                        }
                    };

                    to_add.retain(|x| !config.list.contains(x));

                    database.add_channel_config(guild_id, command_name, to_add).await?;    

                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title(title)
                            .description("Successfully updated list!")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
                }
                "➖" => {
                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title(title.clone())
                            .description("Enter a list of channels to remove from the list")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;

                    let mut to_remove = match msg.author.await_reply(&ctx).timeout(std::time::Duration::from_secs(60)).await {
                        Some(m) => {
                            parse_channels(m.content.clone())
                        }
                        None => {
                            return Ok(());
                        }
                    };

                    let mut remainder = Vec::new();

                    let mut i = 0;

                    while i < to_remove.len() {
                        if !config.list.contains(&to_remove[i]) {
                            remainder.push(to_remove[i]);

                            to_remove.remove(i);
                        }
                        else {
                            i += 1;
                        }
                    }

                    let extra = if remainder.is_empty() { "".to_owned() } else {
                        let mut remainder_string = String::new();
                        
                        for c in remainder {
                            remainder_string.push_str(format!("<#{}>", c).as_str());
                        }

                        format!("The channels {} were not in the list to begin with.", remainder_string)
                    };

                    database.remove_channel_config(guild_id, command_name, to_remove).await?;

                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title(title)
                            .description(format!("Successfully updated list! {}", extra))
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
                }
                _ => {}
            }
        },
        None => {}
    }

    Ok(())
}