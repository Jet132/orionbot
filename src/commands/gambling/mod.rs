mod blackjack;
pub use blackjack::*;

use serenity::framework::standard::macros::group;

#[group]
#[commands(blackjack)]
struct Gambling;