#[derive(Clone)]
pub enum Outcome {
    Successful { // Only when the rob is supposed to end
        message: &'static str,
        nuggets_multiplier: f32,
        final_message: fn(total_time: i32, nuggets: i64) -> String,
    },
    Continue {
        message: &'static str,
        time_cost: i32,
        chained: Option<Scenario>,
    },
    Unsuccessful {
        message: &'static str,
        arrested: bool,
    },
    Chained(Scenario),
}

#[derive(Clone)]
pub struct Choice {
    pub description: &'static str,
    pub emoji_reaction: &'static str,

    pub outcome: fn(random: f32, total_time: i32) -> Outcome,
}

#[derive(Clone)]
pub struct Scenario {
    pub description: &'static str,
    pub choices: &'static [Choice],
}

use serenity::model::prelude::*;

impl Scenario {
    pub fn embed_text(&self) -> String {

        let mut choices = String::new();

        for choice in self.choices.iter() {
            choices.push_str(format!("{} {}\n", choice.emoji_reaction, choice.description).as_str());
        }

        format!("{}\n\n{}", self.description, choices)
    }

    pub fn reactions(&self) -> Vec<ReactionType> {
        let mut vec = Vec::new();

        for choice in self.choices.iter() {
            vec.push(ReactionType::Unicode(choice.emoji_reaction.to_string()));
        }

        vec
    }

    pub fn match_reaction(&self, reaction: String) -> Option<&Choice> {
        for choice in self.choices.iter() {
            if choice.emoji_reaction == reaction {
                return Some(&choice);
            }
        }

        None
    }
}