use super::super::types::*;

pub const ENTER_BANK_INTERMEDIATE: Scenario = Scenario {
    description: "You finally walk up towards the bank, at night. 
    You can try to enter the building through the front, side or back. Which do you choose?",
    choices: &[
        Choice {
            description: "Front",
            emoji_reaction: "⬆️",
            outcome: | rand, _time | {
                if rand < 0.2 { // 20%
                    Outcome::Continue {
                        message: "The front door is locked shut. 
                        While picking the lock, a person walks by and startles you. 
                        Swiftly, you hide nearby. 
                        It turns out it was only a drunken man and they didn't notice anything, however you wasted some time hiding. 
                        You pick the lock again and shortly afterwards enter the building.",
                        time_cost: 3,
                        chained: Some(BANK_FINAL),
                    }
                }
                else if rand < 0.3 { // 10%
                    Outcome::Continue {
                        message: "The front door is locked shut. 
                        While picking the lock, you hear a police siren louder and louder. 
                        Swiftly, you hide nearby. 
                        Luckily they didn't see anything, and you proceed picking the lock and shortly afterwards enter the building.",
                        time_cost: 2,
                        chained: Some(BANK_FINAL),
                    }
                }
                else if rand < 0.8 { // 50%
                    Outcome::Unsuccessful {
                        message: "The front door is locked shut. 
                        While picking the lock, you suddenly hear a car braking nearby.
                        It turns out it was the police, who swiftly arrest you.",
                        arrested: true,
                    }
                }
                else {
                    Outcome::Continue {
                        message: "The front door is locked shut. You pick the lock successfully, and swiftly enter the building.",
                        time_cost: 1,
                        chained: Some(BANK_FINAL),
                    }
                }
            }
        },

        Choice {
            description: "Side",
            emoji_reaction: "⬅️",
            outcome: | rand, _time | {
                if rand < 0.6 { // 60%
                    Outcome::Chained(
                        Scenario {
                            description: "There is a guard patrolling near the side door! 
                            Do you try to quickly sneak by to the back door, or do you try to knock him out and enter through the side?",
                            choices: &[
                                Choice {
                                    description: "Sneak",
                                    emoji_reaction: "⬅️",
                                    outcome: | rand, _time | {
                                        if rand < 0.6 { // 60%
                                            Outcome::Unsuccessful {
                                                message: "The guard notices you and catches you! They swiftly call the police. You are arrested!",
                                                arrested: true,
                                            }
                                        }
                                        else {
                                            Outcome::Continue {
                                                message: "You successfully sneak past the guard unnoticed, and enter through the back.",
                                                time_cost: 1,
                                                chained: Some(BANK_FINAL),
                                            }
                                        }
                                    }
                                },
                                Choice {
                                    description: "Knock guard out",
                                    emoji_reaction: "🔧",
                                    outcome: | rand, _time | {
                                        if rand < 0.6 { // 60%
                                            Outcome::Unsuccessful {
                                                message: "The guard notices you and catches you! They swiftly call the police. You are arrested!",
                                                arrested: true,
                                            }
                                        }
                                        else {
                                            Outcome::Continue {
                                                message: "You hit the guard in the back of the head with a blunt object. He isn't dead, but he should be unconscious for long enough. You enter through the side.",
                                                time_cost: 2,
                                                chained: Some(BANK_NO_GUARD_FINAL),
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    )
                }
                else {
                    Outcome::Continue {
                        message: "You sneak round the side of the building and pick the lock of the side door. Swiftly, you enter the building.",
                        time_cost: 1,
                        chained: Some(BANK_FINAL),
                    }
                }
            }
        },
        Choice {
            description: "Back",
            emoji_reaction: "⬇️",
            outcome: | rand, _time | {
                if rand < 0.6 { // 60%
                    Outcome::Chained(
                        Scenario {
                            description: "There is a guard patrolling near the back door! 
                            Do you try to quickly sneak by to the side door, or do you try to knock him out and enter through the back?",
                            choices: &[
                                Choice {
                                    description: "Sneak",
                                    emoji_reaction: "⬇️",
                                    outcome: | rand, _time | {
                                        if rand < 0.6 { // 60%
                                            Outcome::Unsuccessful {
                                                message: "The guard notices you and catches you! They swiftly call the police. You are arrested!",
                                                arrested: true,
                                            }
                                        }
                                        else {
                                            Outcome::Continue {
                                                message: "You successfully sneak past the guard unnoticed, and enter through the side.",
                                                time_cost: 1,
                                                chained: Some(BANK_FINAL),
                                            }
                                        }
                                    }
                                },
                                Choice {
                                    description: "Knock guard out",
                                    emoji_reaction: "🔧",
                                    outcome: | rand, _time | {
                                        if rand < 0.6 { // 60%
                                            Outcome::Unsuccessful {
                                                message: "The guard notices you and catches you! They swiftly call the police. You are arrested!",
                                                arrested: true,
                                            }
                                        }
                                        else {
                                            Outcome::Continue {
                                                message: "You hit the guard in the back of the head with a blunt object. He isn't dead, but he should be unconscious for long enough. You enter through the back.",
                                                time_cost: 2,
                                                chained: Some(BANK_NO_GUARD_FINAL),
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    )
                }
                else {
                    Outcome::Continue {
                        message: "You sneak round the side of the building and pick the lock of the back door. Swiftly, you enter the building.",
                        time_cost: 1,
                        chained: Some(BANK_FINAL),
                    }
                }
            }
        }
    ]
};

const BANK_FINAL_MESSAGE: fn(i32, i64) -> String = | time, nuggets | {
    let time_message = if time < 5 {
        "Its almost dawn. You are lucky you weren't caught, with how much time you wasted! "
    } else { "" };

    format!("You successfully robbed a bank! {}You stole {} nuggets!", time_message, nuggets)
};

pub const BANK_FINAL: Scenario = Scenario {
    description: "You finally reach the vault of the bank! How much time should you spend packing your bags?",
    choices: &[
        Choice {
            description: "2 minutes",
            emoji_reaction: "2️⃣",
            outcome: | _rand, time | {
                if time >= 15 {
                    Outcome::Unsuccessful {
                        message: "You linger for only a short while and steal as much as you can in that amount of time. 
                        As you leave the guard, you bump into the guard, and he quickly apprehends you. You are arrested!",
                        arrested: true,
                    }
                }
                else {
                    Outcome::Successful {
                        message: "You linger for only a short while and steal as much as you can in that amount of time.",
                        nuggets_multiplier: 1.2,
                        final_message: BANK_FINAL_MESSAGE,
                    }
                }
            }
        },
        Choice {
            description: "5 minutes",
            emoji_reaction: "5️⃣",
            outcome: | rand, time | {
                if rand < 0.6 || time >= 11 { // 60%
                    Outcome::Unsuccessful {
                        message: "You linger for only a while and steal as much as you can in that amount of time. 
                        As you leave the guard, you bump into the guard, and he quickly apprehends you. You are arrested!",
                        arrested: true,
                    }
                }
                else {
                    Outcome::Successful {
                        message: "You linger for only a while and steal as much as you can in that amount of time.",
                        nuggets_multiplier: 2.2,
                        final_message: BANK_FINAL_MESSAGE,
                    }
                }
            }
        },
        Choice {
            description: "8 minutes",
            emoji_reaction: "8️⃣",
            outcome: | rand, time | {
                if rand < 0.8 || time >= 7 { // 80%
                    Outcome::Unsuccessful {
                        message: "You linger for some time and steal as much as you can. 
                        As you leave the guard, you bump into the guard, and he quickly apprehends you. You are arrested!",
                        arrested: true,
                    }
                }
                else {
                    Outcome::Successful {
                        message: "You linger for some time and steal as much as you can.",
                        nuggets_multiplier: 3.4,
                        final_message: BANK_FINAL_MESSAGE,
                    }
                }
            }
        }
    ]
};

pub const BANK_NO_GUARD_FINAL: Scenario = Scenario {
    description: "You finally reach the vault of the bank! How much time should you spend packing your bags?",
    choices: &[
        Choice {
            description: "2 minutes",
            emoji_reaction: "2️⃣",
            outcome: | _rand, _time | {
                Outcome::Successful {
                    message: "You linger for only a short while and steal as much as you can in that amount of time.",
                    nuggets_multiplier: 1.2,
                    final_message: BANK_FINAL_MESSAGE,
                }
            }
        },
        Choice {
            description: "5 minutes",
            emoji_reaction: "5️⃣",
            outcome: | _rand, _time | {
                Outcome::Successful {
                    message: "You linger for only a while and steal as much as you can in that amount of time.",
                    nuggets_multiplier: 2.2,
                    final_message: BANK_FINAL_MESSAGE,
                }
            }
        },
        Choice {
            description: "8 minutes",
            emoji_reaction: "8️⃣",
            outcome: | _rand, _time | {
                Outcome::Successful {
                    message: "You linger for some time and steal as much as you can.",
                    nuggets_multiplier: 3.4,
                    final_message: BANK_FINAL_MESSAGE,
                }
            }
        }
    ]
};