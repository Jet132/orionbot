use super::super::types::*;

pub const JUNCTION_TWO_INITIAL: Scenario = Scenario {
    description: "You are approaching a junction with two possible paths: left and right. Which way do you go?",
    choices: &[
        Choice {
            description: "Left",
            emoji_reaction: "⬅️",
            outcome: | rand, _ | {
                if rand < 0.2 {
                    Outcome::Continue {
                        message: "The left path was the incorrect way, and you wasted some time. Eventually, you got back on track.",
                        time_cost: 3,
                        chained: None,
                    }
                }
                else {
                    Outcome::Continue {
                        message: "You walked the left path.",
                        time_cost: 1,
                        chained: None,
                    }
                }
            }
        },

        Choice {
            description: "Right",
            emoji_reaction: "➡️",
            outcome: | rand, _ | {
                if rand < 0.2 {
                    Outcome::Continue {
                        message: "The right path was the incorrect way, and you wasted some time. Eventually, you got back on track.",
                        time_cost: 3,
                        chained: None,
                    }
                }
                else {
                    Outcome::Continue {
                        message: "You walked the right path.",
                        time_cost: 1,
                        chained: None,
                    }
                }
            }
        }
    ]
};

pub const JUNCTION_THREE_INITIAL: Scenario = Scenario {
    description: "You are approaching a junction with three possible paths: left, forwards and right. Which way do you go?",
    choices: &[
        Choice {
            description: "Left",
            emoji_reaction: "⬅️",
            outcome: | rand, _ | {
                if rand < 0.2 {
                    Outcome::Continue {
                        message: "The left path was the incorrect way, and you wasted some time. Eventually, you got back on track.",
                        time_cost: 4,
                        chained: None,
                    }
                }
                else {
                    Outcome::Continue {
                        message: "You walked the left path.",
                        time_cost: 1,
                        chained: None,
                    }
                }
            }
        },

        Choice {
            description: "Forwards",
            emoji_reaction: "⬆️",
            outcome: | rand, _ | {
                if rand < 0.2 {
                    Outcome::Continue {
                        message: "The forwards path was the incorrect way, and you wasted some time. Eventually, you got back on track.",
                        time_cost: 4,
                        chained: None,
                    }
                }
                else {
                    Outcome::Continue {
                        message: "You walked the forwards path.",
                        time_cost: 1,
                        chained: None,
                    }
                }
            }
        },

        Choice {
            description: "Right",
            emoji_reaction: "➡️",
            outcome: | rand, _ | {
                if rand < 0.2 {
                    Outcome::Continue {
                        message: "The right path was the incorrect way, and you wasted some time. Eventually, you got back on track.",
                        time_cost: 4,
                        chained: None,
                    }
                }
                else {
                    Outcome::Continue {
                        message: "You walked the right path.",
                        time_cost: 1,
                        chained: None,
                    }
                }
            }
        }
    ]
};