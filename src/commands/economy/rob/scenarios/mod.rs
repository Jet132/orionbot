mod junction;
pub use junction::*;

mod city_block;
pub use city_block::*;

mod bank;
pub use bank::*;

pub const INITIAL: [super::Scenario; 2] = [
    JUNCTION_TWO_INITIAL,
    JUNCTION_THREE_INITIAL
];

pub const COMMON_INTERMEDIATE: [super::Scenario; 1] = [
    CITY_BLOCK_INTERMEDIATE
];

pub const BANK_STAGE: [super::Scenario; 1] = [
    ENTER_BANK_INTERMEDIATE
];