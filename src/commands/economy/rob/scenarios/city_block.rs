use super::super::types::*;

pub const CITY_BLOCK_INTERMEDIATE: Scenario = Scenario {
    description: "You see the police ahead, in the direction you are going. Do you try to sneak past them, or do you walk into a side alley to completely avoid them?",
    choices: &[
        Choice {
            description: "Sneak",
            emoji_reaction: "⬆️",
            outcome: | rand, _ | {
                if rand < 0.5 {
                    Outcome::Continue {
                        message: "The police stopped you, and asked if you saw something suspicious. You were left free to go, but it took some time.",
                        time_cost: 4,
                        chained: None,
                    }
                }
                else if rand < 0.6 { // 10%
                    Outcome::Unsuccessful {
                        message: "The police stopped you to talk about stolen goods in the area. Panicked, you run away, but get stopped a few metres away.",
                        arrested: true,
                    }
                }
                else if rand < 0.7 { // 10%
                    Outcome::Continue {
                        message: "The police stopped you to talk about stolen goods in the area. Panicked, you run away, and hide for a short time.",
                        time_cost: 8,
                        chained: None,
                    }
                }
                else {
                    Outcome::Continue {
                        message: "You managed to sneak past the police unnoticed, and saved some time!",
                        time_cost: 1, 
                        chained: None,
                    }
                }
            }
        },

        Choice {
            description: "Side alley",
            emoji_reaction: "➡️",
            outcome: | rand, _ | {
                if rand < 0.2 {
                    Outcome::Continue {
                        message: "You took the side alley to avoid the cops. It turns out it wasn't so easy to get back on track, and you waste more time than expected.",
                        time_cost: 5,
                        chained: None,
                    }
                }
                else {
                    Outcome::Continue {
                        message: "You took the side alley to avoid the cops. It took a short while before you managed to get back on track.",
                        time_cost: 3,
                        chained: None,
                    }
                }
            }
        }
    ]
};