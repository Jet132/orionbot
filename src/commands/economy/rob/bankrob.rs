use serenity::{

    framework::standard::{
        CommandResult,
        macros::command
    },
};

use rand::prelude::*;

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use chrono::prelude::*;
use chrono::Duration;

use crate::cache::*;
use crate::db::*;

use super::types::*;

use std::collections::VecDeque;

use nebbot_utils::*;

use diesel::SaveChangesDsl;

#[command]
#[only_in(guilds)]
#[required_permissions("ADMINISTRATOR")]
#[description("Attempt to steal some Nuggets! :gem: from the bank")]
async fn bankrob(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let mut random = rand::rngs::OsRng;

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    if bot_cache.lock().await.robbing.contains(&discord_id) {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Bank Rob")
                .description("You are already robbing somewhere else!")
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;

        return Ok(());
    }

    let mut db_user = match database.get_user(discord_id, guild_id).await? {
        Some(u) => u,
        None => {
            let default = DbUser::default(discord_id, guild_id);
            
            database.add_user(default.clone()).await?;

            default
        }
    };

    {
        let duration = Utc::now() - db_user.last_bankrob;

        if db_user.arrested {
            if duration <= Duration::hours(super::ARRESTED_COOLDOWN) {

                let time_next = Duration::hours(super::ARRESTED_COOLDOWN) - duration;
    
                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Rob")
                        .description(format!("You are arrested! You need to wait {}",
                            human_readable(time_next)))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;
    
                return Ok(());
            }
        }
        else {
            if duration <= Duration::hours(super::BANKROB_COOLDOWN) {

                let time_next = Duration::hours(super::BANKROB_COOLDOWN) - duration;
    
                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Rob")
                        .description(format!("You need to wait {}",
                            human_readable(time_next)))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;
    
                return Ok(());
            }
        }        
    }

    {
        let mut lock = bot_cache.lock().await;
        lock.robbing.push(discord_id);
    }

    db_user.last_bankrob = Utc::now();

    let _: DbUser = db_user.save_changes(&*database.conn.lock().await)?;

    let mut finished = false;

    let mut time: i32 = 0;
    let mut nuggets: i64 = random.gen_range(100, 500);
    let mut is_arrested = false;
    let mut success = false;

    // ---- INITIAL ----

    let mut current = {
        let list = super::INITIAL.clone();
        list.choose(&mut random).unwrap().clone()
    };

    let stage_1 = {
        let list = super::COMMON_INTERMEDIATE.clone();
        list.choose(&mut random).unwrap().clone()
    };

    let stage_2 = {
        let list = super::BANK_STAGE.clone();
        list.choose(&mut random).unwrap().clone()
    };

    let mut queue = VecDeque::new();
    queue.push_back(stage_1);
    queue.push_back(stage_2);

    let mut game: Message;

    let mut final_message: fn(i32, i64) -> String = | _, _ | "".to_owned();
    
    while !finished {

        game = msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Bank Rob")
                .description(current.embed_text())
                .colour(Colour::from_rgb(52, 152, 219))
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            })
        }).await?;
    
        for reaction in current.reactions() {
            game.react(&ctx.http, reaction).await?;
        }

        let mut start_time = Utc::now();
        let mut timeout = std::time::Duration::from_secs(30);

        let old_description = current.description;
        let outcome: Outcome;

        loop {
            match game.await_reaction(&ctx).timeout(timeout).author_id(discord_id).await {
                Some(reaction) => {
                    let emoji = &reaction.as_inner_ref().emoji;

                    match current.match_reaction(emoji.as_data()) {
                        Some(choice) => {
                            game.delete_reactions(&ctx.http).await?;

                            outcome = (choice.outcome)(random.gen(), time);

                            break;
                        }
                        None => {
                            let now = Utc::now();
                            timeout -= (now - start_time).to_std().unwrap();
                            start_time = now;
                        }
                    }
                }

                None => {
                    game.edit(&ctx.http, |m| {
                        m
                        .embed(|e| {
                            e
                            .title("Bank Rob")
                            .description(format!("~~{}~~\n\nTime out.", current.embed_text()))
                            .colour(Colour::from_rgb(52, 152, 219))
                            .footer(|f| f
                                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                                .text(msg.author.tag())
                            )
                        })
                    }).await?;

                    {
                        let mut lock = bot_cache.lock().await;
                
                        let index = lock.robbing.iter().position(|&x| x == discord_id).expect("Could not find discord id in rob");
                        lock.robbing.remove(index);
                    }

                    return Ok(());
                }
            }
        }

        let outcome_message = match outcome {

            Outcome::Successful { message, nuggets_multiplier, final_message: fm } => {
                nuggets = (nuggets as f32 * nuggets_multiplier).round() as i64;

                success = true;
                finished = true;

                final_message = fm;

                message
            }
            Outcome::Unsuccessful { message, arrested } => {

                is_arrested = arrested;
                finished = true;

                message
            }
            Outcome::Continue { message, time_cost, chained } => {

                if let Some(c) = chained {
                    current = c.clone();
                }
                else {
                    match queue.pop_front() {
                        Some(s) => { current = s; },
                        None => { error!("Rob queue empty"); return Ok(()); }
                    }
                }

                time += time_cost;

                message
            }
            Outcome::Chained(scenario) => {
                current = scenario.clone();

                ""
            }
        };

        game.edit(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Bank Rob")
                .description(format!("{}\n\n{}", old_description, outcome_message))
                .colour(Colour::from_rgb(52, 152, 219))
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            })
        }).await?;

        if time >= 20 {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Bank Rob")
                    .description("Its dawn already! You wasted too much time.")
                    .colour(Colour::from_rgb(52, 152, 219))
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                })
            }).await?;

            {
                let mut lock = bot_cache.lock().await;
        
                let index = lock.robbing.iter().position(|&x| x == discord_id).expect("Could not find discord id in rob");
                lock.robbing.remove(index);
            }

            return Ok(());
        }
    }

    if success {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Bank Rob")
                .description(final_message(time, nuggets))
                .colour(Colour::from_rgb(52, 152, 219))
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            })
        }).await?;
    }

    db_user.arrested = is_arrested;

    if success { db_user.nuggets += nuggets; }

    let _: DbUser = db_user.save_changes(&*database.conn.lock().await)?;    

    {
        let mut lock = bot_cache.lock().await;

        let index = lock.robbing.iter().position(|&x| x == discord_id).expect("Could not find discord id in rob");
        lock.robbing.remove(index);
    }

    Ok(())
}