mod types;
pub use types::*;

mod scenarios;
pub use scenarios::*;

mod bankrob;
pub use bankrob::*;

//pub const ROB_COOLDOWN: i64 = 5; // Minutes
pub const BANKROB_COOLDOWN: i64 = 6; // Hours
pub const ARRESTED_COOLDOWN: i64 = 12; // Hours