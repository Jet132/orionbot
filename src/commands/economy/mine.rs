use serenity::framework::standard::{
    CommandResult, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::utils::Colour;

use chrono::prelude::*;
use chrono::Duration;

use rand::prelude::*;

use crate::db::DbUser;
use nebbot_utils::*;

use diesel::SaveChangesDsl;

#[command]
#[description("Mine some nuggets every 4 hours!")]
#[only_in(guilds)]
async fn mine(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let mut random = rand::rngs::OsRng;

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let mut delta: i64 = 0;
    let gained;

    match database.get_user(discord_id, guild_id).await? {
        Some(mut user) => {
            if user.arrested {

                let duration = Utc::now() - user.last_bankrob;

                if duration <= Duration::hours(super::super::ARRESTED_COOLDOWN) {
                    let time_next = Duration::hours(super::super::ARRESTED_COOLDOWN) - duration;

                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Mine")
                            .description(format!("You are arrested! You need to wait {}",
                                human_readable(time_next)))
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
    
                    return Ok(());

                }
            }

            let duration = Utc::now() - user.last_mine;

            if duration <= Duration::hours(super::MINE_COOLDOWN) {

                let time_next = Duration::hours(super::MINE_COOLDOWN) - duration;

                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Mine")
                        .description(format!("You need to wait {}",
                            human_readable(time_next)))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;

                return Ok(());
            }

            if !user.has_pickaxe {
                if user.nuggets < super::PICKAXE_PRICE {
                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Mine")
                            .description(format!("You need a pickaxe, but you are too poor to afford it. Come back when you have {} Nuggets! :gem:", super::PICKAXE_PRICE))
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;

                    return Ok(());
                }

                let mut message = msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Mine")
                        .description(format!("You need a pickaxe. Would you like to buy one for {} Nuggets! :gem: ?", super::PICKAXE_PRICE))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;

                message.react(&ctx.http, ReactionType::Unicode("✅".to_string())).await?;
                message.react(&ctx.http, ReactionType::Unicode("❌".to_string())).await?;

                let mut start_time = Utc::now();
                let mut timeout = std::time::Duration::from_secs(30);

                loop {
                    match message.await_reaction(&ctx).timeout(timeout).author_id(discord_id).await {
                        Some(reaction) => {
                            let emoji = &reaction.as_inner_ref().emoji;

                            match emoji.as_data().as_str() {
                                "✅" => {
                                    delta -= super::PICKAXE_PRICE;

                                    break;
                                }
                                "❌" => {
                                    message.edit(&ctx.http, |m| {
                                        m
                                        .embed(|e| {
                                            e
                                            .title("Mine")
                                            .description(format!("~~You need a pickaxe. Would you like to buy one for {} Nuggets! :gem: ?~~\n\nCancelled.", super::PICKAXE_PRICE))
                                            .colour(Colour::from_rgb(52, 152, 219))
                                        })
                                    }).await?;

                                    return Ok(());
                                }

                                _ => {
                                    let now = Utc::now();
                                    timeout -= (now - start_time).to_std().unwrap();
                                    start_time = now;
                                }
                            }
                        }

                        None => {
                            message.edit(&ctx.http, |m| {
                                m
                                .embed(|e| {
                                    e
                                    .title("Mine")
                                    .description(format!("~~You need a pickaxe. Would you like to buy one for {} Nuggets! :gem: ?~~\n\nNo response.", super::PICKAXE_PRICE))
                                    .colour(Colour::from_rgb(52, 152, 219))
                                })
                            }).await?;

                            return Ok(());
                        }
                    }
                }
            }

            if user.nuggets + delta < super::MINE_COST {
                user.nuggets += delta;
                user.has_pickaxe = true;

                let _:DbUser = user.save_changes(&*database.conn.lock().await)?;

                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Mine")
                        .description(format!("You need to pay the entrance fee, but you are too poor to afford it. Come back when you have {} Nuggets! :gem:", super::MINE_COST))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;
            }
            else {
                delta -= super::MINE_COST;

                let bracket: f32 = random.gen();

                // 1% chance
                if bracket <= 0.01 {
                    user.nuggets += delta;
                    user.has_pickaxe = false;
                    user.last_mine = Utc::now();
    
                    let _: DbUser = user.save_changes(&*database.conn.lock().await)?;
    
                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Mine")
                            .description("Your pickaxe broke!")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
    
                    return Ok(());
                }
    
                // 3% chance
                else if bracket <= 0.04 {
                    gained = random.gen_range(40, 150);
                }
    
                // 50% chance
                else if bracket <= 0.54 {
                    gained = random.gen_range(150, 300);
                }
    
                // 33% chance
                else if bracket <= 0.87 {
                    gained = random.gen_range(300, 500);
                }

                // 10% chance
                else if bracket <= 0.97 {
                    gained = random.gen_range(500, 800);
                }

                // 3% chance
                else {
                    gained = random.gen_range(800, 2000);
                }

                delta += gained;

                user.nuggets += delta;
                user.has_pickaxe = true;
                user.last_mine = Utc::now();

                let _: DbUser = user.save_changes(&*database.conn.lock().await)?;

                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Mine")
                        .description(format!("You mined {} Nuggets! :gem: You now have {} Nuggets! :gem:",
                            gained, user.nuggets + delta))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;
            }
        }

        None => {
            let default = DbUser::default(discord_id, guild_id);
            database.add_user(default).await?;

            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Mine")
                    .description(format!("You need a pickaxe, but you are too poor to afford it. Come back when you have {} Nuggets! :gem:", super::PICKAXE_PRICE))
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        }
    }

    Ok(())
}