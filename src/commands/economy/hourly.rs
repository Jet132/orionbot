use serenity::framework::standard::{
    CommandResult, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::utils::Colour;

use chrono::prelude::*;
use chrono::Duration;

use crate::db::*;
use diesel::SaveChangesDsl;

use nebbot_utils::*;

#[command]
#[description("Gain 20 Nuggets! :gem: every hour")]
#[only_in(guilds)]
async fn hourly(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let nuggets = match database.get_user(discord_id, guild_id).await? {
        Some(mut user) => {
            if user.arrested {

                let duration = Utc::now() - user.last_bankrob;

                if duration <= Duration::hours(super::super::ARRESTED_COOLDOWN) {
                    let time_next = Duration::hours(super::super::ARRESTED_COOLDOWN) - duration;

                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Hourly")
                            .description(format!("You are arrested! You need to wait {}",
                                human_readable(time_next)))
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
    
                    return Ok(());

                }
            }

            let duration = Utc::now() - user.last_hourly;

            if duration <= Duration::hours(1) {

                let time_next = Duration::hours(1) - duration;

                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Hourly")
                        .description(format!("You need to wait {}",
                            human_readable(time_next)))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;

                return Ok(());
            }

            user.nuggets += super::HOURLY;
            user.last_hourly = Utc::now();

            let _: DbUser = user.save_changes(&*database.conn.lock().await)?;

            user.nuggets
        }

        None => {
            let mut default = DbUser::default(discord_id, guild_id);

            default.nuggets += super::HOURLY;
            default.last_hourly = Utc::now();

            database.add_user(default.clone()).await?;

            default.nuggets
        }
    };

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Hourly")
            .description(format!("Gained {} Nuggets! :gem: You now have {} Nuggets! :gem:", super::HOURLY, nuggets))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await?;

    Ok(())
}