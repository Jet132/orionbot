mod hourly;
use hourly::*;

mod mine;
use mine::*;

mod balance;
use balance::*;

mod pay;
use pay::*;

mod rob;
pub use rob::*;

use serenity::framework::standard::macros::group;

#[group]    
#[commands(hourly, balance, mine, pay, bankrob)]
struct Economy;

pub const MINE_COOLDOWN: i64 = 4; // Hours
pub const PICKAXE_PRICE: i64 = 50;
pub const MINE_COST: i64 = 100;
pub const HOURLY: i64 = 20;