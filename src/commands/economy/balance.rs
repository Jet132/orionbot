use serenity::framework::standard::{
    CommandResult, Args, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::utils::Colour;

use crate::db::DbUser;
use nebbot_utils::*;

#[command]
#[aliases("bal", "b", "nuggets", "n")]
#[description("Check your balance")]
#[only_in(guilds)]
async fn balance(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let cached = ctx.cache.users().await;

    let dc_user = match match_discord_user(&args, cached.clone(), 0) {
        MatchDiscordUser::Other(u) => u,
        MatchDiscordUser::Bot(_) | MatchDiscordUser::NoUserFound |
        MatchDiscordUser::InvalidForm(_) => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Balance")
                    .description("Invalid User Argument")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        },
        MatchDiscordUser::SameAsHost | 
        MatchDiscordUser::NoArgument => msg.author.clone(),
    };

    let id = *dc_user.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    match database.get_user(id, guild_id).await? {
        Some(user) => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Balance")
                    .description(format!("{} has {} Nuggets! :gem:", dc_user.mention(), user.nuggets))
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;
        }

        None => {
            let default = DbUser::default(id, guild_id);
            database.add_user(default).await?;

            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Balance")
                    .description(format!("{} has 0 Nuggets! :gem:", dc_user.mention()))
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;
        }
    }

    Ok(())
}