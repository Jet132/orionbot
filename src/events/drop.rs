use serenity:: {
    framework::standard::macros::hook,

    collector::MessageCollectorBuilder,
    futures::stream::StreamExt,

    model::channel::Message
};

use rand::prelude::*;

use std::cell::Cell;

use nebbot_utils::*;

use crate::cache::*;
use crate::db::*;

use chrono::prelude::*;
use chrono::Duration;

use serenity::prelude::*;
use serenity::utils::Colour;

use diesel::SaveChangesDsl;

const MIN_RATE: f32 = 5.0; // Messages per Minute
const MIN_TIME: i64 = 5; // Minutes
const CLAIM_TIME: u64 = 10; // Seconds
const ANTI_SPAM_DELAY: i64 = 3; // Seconds

#[hook]
pub async fn on_message(ctx: &Context, msg: &Message) {
    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
    let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let mut random = rand::rngs::OsRng;

    if msg.is_private() {
        return;
    }

    let channel_id = *msg.channel_id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    if msg.author.bot {
        //debug!("Drop - is bot");
        return;
    }

    match database.is_command_enabled(guild_id, channel_id, "drop").await {
        Ok(b) => if !b {
            return;
        },
        Err(e) => {
            error!("Random Nugget Drop Database Error: {:?}", e);
            return;
        }
    }

    let time_difference;
    let new_messages;

    {
        let mut lock = bot_cache.lock().await;

        if lock.loot_dropping {
            //debug!("Drop - is dropping");
            return;
        }

        match lock.nugget_drop.get(&channel_id) {
            Some(drop) => {

                if Utc::now() - drop.time_last_message.get() < Duration::seconds(ANTI_SPAM_DELAY) {
                    return;
                }

                new_messages = drop.messages.get() + 1;

                drop.messages.set(new_messages);
                drop.time_last_message.set(Utc::now());

                time_difference = Utc::now() - drop.time_start.get();
            },
            None => {
                lock.nugget_drop.insert(channel_id, 
                    NuggetDrop {
                        time_start: Cell::new(Utc::now()),
                        time_last_message: Cell::new(Utc::now()),
                        messages: Cell::new(1),
                    });

                return;
            }
        }
    }

    if time_difference < Duration::minutes(MIN_TIME) {
        return;
    }

    let rate = new_messages as f32 / time_difference.num_minutes() as f32;

    if rate < MIN_RATE {

        let mut lock = bot_cache.lock().await;

        lock.nugget_drop.remove(&channel_id);

        //debug!("Drop rate: {}; Channel ID: {}; Messages: {}; Minutes: {}", rate, channel_id, new_messages, time_difference.num_minutes());

        return;
    }

    {
        let mut lock = bot_cache.lock().await;

        lock.loot_dropping = true;
    }

    let nuggets = 20 + (random.gen::<f32>() * 8.0 * rate * (10.0 * rate).log10()).round() as i64;

    tokio::time::delay_for(tokio::time::Duration::from_secs(20)).await;

    let mut claim_message = match msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Random Nuggets Drop! 💎")
            .description(format!("Quick! Claim {} Nuggets! :gem:, by replying with `claim`!",
                nuggets))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await {
        Ok(m) => m,
        Err(e) => {
            error!("Random Nugget Drop Message -- {}", e);
            return;
        }
    };

    let mut collector = MessageCollectorBuilder::new(&ctx)
        .channel_id(msg.channel_id)
        .timeout(std::time::Duration::from_secs(CLAIM_TIME))
        .await;

    loop {
        let message = match collector.next().await{
            Some(m) => m,
            None => {
                match claim_message.edit(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Random Nuggets Drop! 💎")
                        .description(format!("~~Quick! Claim {} Nuggets! :gem:, by replying with `claim`!~~ Time's up!",
                            nuggets))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await {
                    Ok(_) => {},
                    Err(e) => {
                        error!("Random Nugget Drop Edit Message -- {}", e);
                        return;
                    }
                }

                break;
            }
        };

        if message.author.bot { continue; }

        if !message.content.trim().to_lowercase().starts_with("claim") { continue; }

        let user_id = *message.author.id.as_u64();

        let total_nuggets = match database.get_user(user_id, guild_id).await {
            Ok(optn) => match optn {
                Some(mut u) => {
                    u.nuggets += nuggets;

                    let _: DbUser = match u.save_changes(&*database.conn.lock().await) {
                        Ok(u) => u,
                        Err(e) => {
                            error!("Update User Random Loot Drop -- {}", e);
                            return;
                        }
                    };

                    u.nuggets
                },
                None => {
                    let mut default = DbUser::default(user_id, guild_id);

                    default.nuggets += nuggets;

                    match database.add_user(default.clone()).await {
                        Ok(_) => {},
                        Err(e) => {
                            error!("Add Default User Random Loot Drop -- {}", e);
                            return;
                        }
                    }

                    default.nuggets
                }
            },

            Err(e) => {
                error!("Get User Random Loot Drop -- {}", e);
                return;
            }
        };

        match msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Random Nuggets Drop! 💎")
                .description(format!("Success! {} has claimed {} Nuggets! :gem: They now have {} Nuggets! :gem:",
                    message.author.mention(), nuggets, total_nuggets))
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await {
            Ok(_) => {},
            Err(e) => {
                error!("Random Nugget Drop Success Message -- {}", e);
                return;
            }
        }

        break;
    }

    let mut lock = bot_cache.lock().await;

    lock.nugget_drop.remove(&channel_id);

    lock.loot_dropping = false;
}