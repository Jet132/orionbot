use serenity:: {
    async_trait,

    cache::Cache,
    http::client::Http,

    client::bridge::gateway::event::ShardStageUpdateEvent,
    gateway::ConnectionStage,

    model::{
        gateway::Ready,
        event::ResumedEvent,
    },
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use std::sync::Arc;

use nebbot_utils::*;

mod drop;
pub use drop::*;

use crate::cache::*;
use crate::db::*;

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);

        let data = ctx.data.read().await;
        let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
        let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

        cache_roles(ctx.http.clone(), ctx.cache.clone(), database.clone(), bot_cache.clone()).await;
    }

    async fn resume(&self, _: Context, resume: ResumedEvent) {
        info!("Resumed! Trace: {:?}",  resume.trace);
    }

    async fn shard_stage_update(&self, _ctx: Context, update: ShardStageUpdateEvent) {
        
        info!("Shard {} update event: Old: {:?}; New: {:?}", update.shard_id, update.old, update.new);

        if let ConnectionStage::Connecting = update.new {
            warn!("Shard {} has disconnected! Reconnecting...", update.shard_id);
        }

        if let ConnectionStage::Resuming = update.new {
            warn!("Resuming Shard {}...", update.shard_id);
        }
    }
}

async fn cache_roles(http: Arc<Http>, cache: Arc<Cache>, db: Arc<Db>, bot_cache: Arc<Mutex<BotCache>>) {
    for guild_id in cache.guilds().await {

        let guild_id_u64 = *guild_id.as_u64();

        let db_guild = match db.get_guild(guild_id_u64).await {
            Ok(optn_guild) => match optn_guild {
                Some(g) => g,
                None => return,
            }
            Err(e) => {
                error!("Database error get guild: {:?}", e);
                return;
            }
        };

        if db_guild.richest_role.0 != 0 {
            let nuggets = match db.select_top_10_users(guild_id_u64).await {
                Ok(users) => users,
                Err(e) => {
                    error!("Database error select top 10 users: {:?}", e);
                    return;
                }
            };

            if let Some(u) = nuggets.first() {
                bot_cache.lock().await.richest.insert(guild_id_u64, u.discord_id.0);
        
                let mut roles = guild_id.member(&http, &UserId(u.discord_id.0)).await.unwrap().roles;
        
                let role_id = RoleId(db_guild.richest_role.0);

                if !roles.contains(&role_id) {
                    roles.push(role_id);
        
                    match guild_id.edit_member(&http, u.discord_id.0, |m| {
                        m.roles(roles)
                    }).await {
                        Ok(_) => {},
                        Err(e) => {
                            error!("Failed to add Richest role for {}: {:?}", u.discord_id.0, e);
                        }
                    };
                }
            }
        }
    
        if db_guild.longest_spin_role.0 != 0 {
            let spinners = match db.select_top_10_spinners(guild_id_u64).await {
                Ok(users) => users,
                Err(e) => {
                    error!("Database error select top 10 spinners: {:?}", e);
                    return;
                }
            };
        
            if let Some(u) = spinners.first() {
                bot_cache.lock().await.longest_spin.insert(guild_id_u64, u.discord_id.0);
        
                let mut roles = guild_id.member(&http, &UserId(u.discord_id.0)).await.unwrap().roles;
        
                let role_id = RoleId(db_guild.longest_spin_role.0);

                if !roles.contains(&role_id) {
                    roles.push(role_id);
        
                    match guild_id.edit_member(&http, u.discord_id.0, |m| {
                        m.roles(roles)
                    }).await {
                        Ok(_) => {},
                        Err(e) => {
                            error!("Failed to add Longest Spin role for {}: {:?}", u.discord_id.0, e);
                        }
                    };
                }
            }
        }        
    }
}