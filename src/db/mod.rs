#![allow(dead_code)]

pub mod schema;

mod types;
pub use types::*;

mod users;
pub use users::*;

mod guilds;
pub use guilds::*;

mod guild_config;
pub use guild_config::*;

mod spinners;
pub use spinners::*;

use diesel::prelude::*;

use tokio::sync::Mutex;

pub struct Db {
    pub conn: Mutex<PgConnection>,
}

impl Db {
    pub async fn new() -> Db {
        let config = std::env::var("DATABASE_URL").expect("Config required in environmental variable DATABASE_URL.");
        let conn = PgConnection::establish(&config).expect("Could not connect to database.");

        Db { conn: Mutex::new(conn) }
    }
}
