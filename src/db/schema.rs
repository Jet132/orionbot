table! {
    guild_channel_config (guild_id, channel_id, command_name) {
        guild_id -> Text,
        channel_id -> Text,
        command_name -> Text,
    }
}

table! {
    guild_command_config (guild_id, command_name) {
        guild_id -> Text,
        command_name -> Text,
        is_whitelist -> Int2,
    }
}

table! {
    guilds (guild_id) {
        guild_id -> Text,
        richest_role -> Text,
        longest_spin_role -> Text,
    }
}

table! {
    spinners (discord_id, guild_id) {
        discord_id -> Text,
        channel_id -> Text,
        knocked_user_id -> Text,
        duration -> Int8,
        guild_id -> Text,
    }
}

table! {
    users (discord_id, guild_id) {
        discord_id -> Text,
        nuggets -> Int8,
        last_mine -> Timestamptz,
        last_hourly -> Timestamptz,
        has_pickaxe -> Bool,
        last_spin -> Timestamptz,
        arrested -> Bool,
        last_bankrob -> Timestamptz,
        guild_id -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    guild_channel_config,
    guild_command_config,
    guilds,
    spinners,
    users,
);
