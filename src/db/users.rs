use chrono::{ DateTime, Utc, TimeZone };

use crate::db::schema::users;
use diesel::prelude::*;

use crate::db::*;

#[derive(Clone, Debug, Queryable, Insertable, AsChangeset, Identifiable)]
#[primary_key(discord_id, guild_id)]
#[table_name = "users"]
pub struct DbUser {
    pub discord_id: TextU64,
    pub nuggets: i64,
    pub last_mine: DateTime<Utc>,
    pub last_hourly: DateTime<Utc>,
    pub has_pickaxe: bool,
    pub last_spin: DateTime<Utc>,
    pub arrested: bool,
    pub last_bankrob: DateTime<Utc>,
    pub guild_id: TextU64,
}

impl DbUser {
    pub fn default(discord_id: u64, guild_id: u64) -> Self {
        DbUser {
            discord_id: TextU64(discord_id),
            nuggets: 0,
            last_mine: Utc.timestamp(0, 0),
            last_hourly: Utc.timestamp(0, 0),
            last_spin: Utc.timestamp(0, 0),
            has_pickaxe: false,
            arrested: false,
            last_bankrob: Utc.timestamp(0, 0),
            guild_id: TextU64(guild_id),
        }
    }
}

impl Db {
    pub async fn get_user(&self, discord_id: u64, guild_id: u64) -> QueryResult<Option<DbUser>> {
        users::table.find((discord_id.to_string(), guild_id.to_string()))
            .first(&*self.conn.lock().await).optional()
    }

    pub async fn add_user(&self, user: DbUser) -> QueryResult<()> {
        diesel::insert_into(users::table).values(&user).execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn select_top_10_users(&self, guild_id: u64) -> QueryResult<Vec<DbUser>> {
        users::table.filter(users::guild_id.eq(TextU64(guild_id)))
            .limit(10).order(users::nuggets.desc()).load(&*self.conn.lock().await)
    }
}