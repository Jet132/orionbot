use diesel::prelude::*;
use diesel::sql_types::{ Text, SmallInt };
use diesel::serialize::{ ToSql, Output };
use diesel::deserialize::{ FromSql };
use diesel::backend::Backend;

use std::io::prelude::*;

#[derive(Debug, Clone, AsExpression, PartialEq, Eq, Hash)]
#[sql_type = "Text"]
pub struct TextU64(
    #[diesel(deserialize_as = "String")]
    pub u64
);

impl From<u64> for TextU64 {
    fn from(f: u64) -> Self {
        TextU64(f)
    }
}

impl<DB, ST> Queryable<ST, DB> for TextU64
where
    DB: Backend,
    String: Queryable<ST, DB>,
{
    type Row = <String as Queryable<ST, DB>>::Row;

    fn build(row: Self::Row) -> Self {
        TextU64(String::build(row).parse().expect("Could not convert String to u64"))
    }
}

impl<Db> ToSql<Text, Db> for TextU64
where 
    Db: Backend,
    String: ToSql<Text, Db>
{
    fn to_sql<W: Write>(&self, out: &mut Output<W, Db>) -> diesel::serialize::Result {
        self.0.to_string().to_sql(out)
    }
}

impl<Db> FromSql<Text, Db> for TextU64
where 
    Db: Backend,
    String: FromSql<Text, Db>
{
    fn from_sql(bytes: Option<&Db::RawValue>) -> diesel::deserialize::Result<Self> {
        Ok(TextU64(String::from_sql(bytes)?.parse()?))
    }
}

#[repr(i16)]
#[derive(Clone, Debug, AsExpression, PartialEq, Eq, Hash)]
#[sql_type = "SmallInt"]
pub enum Whitelist {
    Disabled,
    Whitelist,
    Blacklist
}

impl Whitelist {
    pub fn from_i16(i: i16) -> Whitelist {
        match i {
            0 => Whitelist::Disabled,
            1 => Whitelist::Whitelist,
            2 => Whitelist::Blacklist,
            _ => Whitelist::Disabled,
        }
    }

    pub fn to_i16(&self) -> i16 {
        match self {
            Whitelist::Disabled => 0,
            Whitelist::Whitelist => 1,
            Whitelist::Blacklist => 2,
        }
    }
}

impl std::fmt::Display for Whitelist {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Whitelist::Disabled => "Disabled",
            Whitelist::Whitelist => "Whitelist",
            Whitelist::Blacklist => "Blacklist",
        })
    }
}

impl<DB, ST> Queryable<ST, DB> for Whitelist
where
    DB: Backend,
    i16: Queryable<ST, DB>,
{
    type Row = <i16 as Queryable<ST, DB>>::Row;

    fn build(row: Self::Row) -> Self {
        Whitelist::from_i16(i16::build(row))
    }
}

impl<Db> ToSql<SmallInt, Db> for Whitelist
where 
    Db: Backend,
    i16: ToSql<SmallInt, Db>
{
    fn to_sql<W: Write>(&self, out: &mut Output<W, Db>) -> diesel::serialize::Result {
        self.to_i16().to_sql(out)
    }
}

impl<Db> FromSql<SmallInt, Db> for Whitelist
where 
    Db: Backend,
    i16: FromSql<SmallInt, Db>
{
    fn from_sql(bytes: Option<&Db::RawValue>) -> diesel::deserialize::Result<Self> {
        Ok(Whitelist::from_i16(i16::from_sql(bytes)?))
    }
}