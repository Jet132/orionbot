use crate::db::schema::{ guild_channel_config, guild_command_config };
use diesel::prelude::*;

use crate::db::*;

use std::collections::HashMap;
use std::cell::RefCell;

#[derive(Clone, Debug, Queryable, Insertable, Identifiable)]
#[primary_key(guild_id, channel_id, command_name)]
#[table_name = "guild_channel_config"]
pub struct DbGuildChannelConfig {
    pub guild_id: TextU64,
    pub channel_id: TextU64,
    pub command_name: String,
}

impl DbGuildChannelConfig {
    pub fn new(guild_id: u64, channel_id: u64, command_name: &str) -> Self {
        DbGuildChannelConfig {
            guild_id: TextU64(guild_id),
            channel_id: TextU64(channel_id),
            command_name: command_name.to_owned()
        }
    }
}

#[derive(Clone, Debug, Queryable, AsChangeset, Insertable, Identifiable)]
#[primary_key(guild_id, command_name)]
#[table_name = "guild_command_config"]
pub struct DbGuildCommandConfig {
    pub guild_id: TextU64,
    pub command_name: String,
    pub is_whitelist: Whitelist,
}

impl DbGuildCommandConfig {
    pub fn new(guild_id: u64, command_name: &str, is_whitelist: Whitelist) -> Self {
        DbGuildCommandConfig {
            guild_id: TextU64(guild_id),
            command_name: command_name.to_owned(),
            is_whitelist
        }
    }
}

#[derive(Clone, Debug)]
pub struct Configuration {
    pub list_type: Whitelist,
    pub list: Vec<u64>,
}

pub type GuildConfiguration = HashMap<String, Configuration>;

type ChannelConfig = HashMap<String, RefCell<Vec<DbGuildChannelConfig>>>;
type CommandConfig = Vec<DbGuildCommandConfig>;


impl Db {
    pub async fn get_whitelists(&self, guild_id: u64) -> QueryResult<GuildConfiguration> {
        let channel_config: ChannelConfig = {
            let configs = guild_channel_config::table
            .filter(guild_channel_config::guild_id.eq(TextU64(guild_id)))
            .load::<DbGuildChannelConfig>(&*self.conn.lock().await)?;

            let mut hashmap = HashMap::new();

            for c in configs {
                if !hashmap.contains_key(&c.command_name) {
                    hashmap.insert(c.command_name.clone(), RefCell::new(vec![c]));
                }
                else {
                    let mut vec = hashmap[&c.command_name].borrow_mut();
                    
                    vec.push(c);
                }
            }

            hashmap
        };

        let command_config: CommandConfig = guild_command_config::table
            .filter(guild_command_config::guild_id.eq(TextU64(guild_id)))
            .load::<DbGuildCommandConfig>(&*self.conn.lock().await)?;

        let mut config = HashMap::new();

        for (k, v) in channel_config {
            
            let whitelist = match command_config.iter().find(|&x| x.command_name == k) {
                Some(w) => w.is_whitelist.clone(),
                None => continue,
            };

            let channels: Vec<u64> = v.into_inner().iter().map(|x| x.channel_id.0).collect();

            config.insert(k, Configuration { list_type: whitelist, list: channels });
        }

        Ok(config)

    }

    pub async fn is_command_enabled(&self, guild_id: u64, channel_id: u64, command_name: &str) -> QueryResult<bool> {
        let channel_config: ChannelConfig = {
            let configs = guild_channel_config::table
                .filter(guild_channel_config::guild_id.eq(TextU64(guild_id)))
                .filter(guild_channel_config::command_name.eq_any(vec!["all", "other", &command_name]))
                .load::<DbGuildChannelConfig>(&*self.conn.lock().await)?;

            let mut hashmap = HashMap::new();

            for c in configs {
                if !hashmap.contains_key(&c.command_name) {
                    hashmap.insert(c.command_name.clone(), RefCell::new(vec![c]));
                }
                else {
                    let mut vec = hashmap[&c.command_name].borrow_mut();
                    
                    vec.push(c);
                }
            }

            hashmap
        };

        let command_config: CommandConfig = guild_command_config::table
            .filter(guild_command_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_command_config::command_name.eq_any(vec!["all", "other", &command_name]))
            .load::<DbGuildCommandConfig>(&*self.conn.lock().await)?;

        if let Some(channels_all) = channel_config.get("all") {
            if let Some(command_all) = command_config.iter().find(|&x| x.command_name == "all") {
                match command_all.is_whitelist {
                    Whitelist::Whitelist => {
                        match channels_all.borrow().iter().find(|&x| x.channel_id.0 == channel_id) {
                            Some(_) => return Ok(true),
                            None => return Ok(false),
                        };
                    },
                    Whitelist::Blacklist => {
                        match channels_all.borrow().iter().find(|&x| x.channel_id.0 == channel_id) {
                            Some(_) => return Ok(false),
                            None => return Ok(true),
                        };
                    },
                    _ => {},
                }
            }
        }

        if let Some(channels) = channel_config.get(&command_name.to_owned()) {
            if let Some(command) = command_config.iter().find(|&x| x.command_name == command_name) {
                match command.is_whitelist {
                    Whitelist::Whitelist => {
                        match channels.borrow().iter().find(|&x| x.channel_id.0 == channel_id) {
                            Some(_) => return Ok(true),
                            None => return Ok(false),
                        };
                    },
                    Whitelist::Blacklist => {
                        match channels.borrow().iter().find(|&x| x.channel_id.0 == channel_id) {
                            Some(_) => return Ok(false),
                            None => return Ok(true),
                        };
                    },
                    _ => {},
                }
            }
        }

        if let Some(channels_other) = channel_config.get("other") {
            if let Some(command_other) = command_config.iter().find(|&x| x.command_name == "other") {
                match command_other.is_whitelist {
                    Whitelist::Whitelist => {
                        match channels_other.borrow().iter().find(|&x| x.channel_id.0 == channel_id) {
                            Some(_) => return Ok(true),
                            None => return Ok(false),
                        };
                    },
                    Whitelist::Blacklist => {
                        match channels_other.borrow().iter().find(|&x| x.channel_id.0 == channel_id) {
                            Some(_) => return Ok(false),
                            None => return Ok(true),
                        };
                    },
                    _ => {},
                }
            }
        }

        Ok(true)

    }

    pub async fn clear_config(&self, guild_id: u64, comamnd_name: &str) -> QueryResult<()> {
        diesel::delete(guild_channel_config::table)
            .filter(guild_channel_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_channel_config::command_name.eq(comamnd_name))
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn update_config(&self, guild_id: u64, command_name: &str, updated_is_whitelist: Whitelist) -> QueryResult<()> {
        diesel::update(guild_command_config::table)
            .filter(guild_command_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_command_config::command_name.eq(command_name))
            .set(guild_command_config::is_whitelist.eq(updated_is_whitelist))
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn add_command_config(&self, guild_id: u64, command_name: &str, is_whitelist: Whitelist) -> QueryResult<()> {
        let data = DbGuildCommandConfig::new(guild_id, command_name, is_whitelist);

        diesel::insert_into(guild_command_config::table)
            .values(data)
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn add_channel_config(&self, guild_id: u64, command_name: &str, channels: Vec<u64>) -> QueryResult<()> {

        let mut data = Vec::new();

        for channel in channels {
            data.push(DbGuildChannelConfig::new(guild_id, channel, &command_name));
        }

        diesel::insert_into(guild_channel_config::table)
            .values(data)
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn remove_channel_config(&self, guild_id: u64, command_name: &str, channels: Vec<u64>) -> QueryResult<()> {
        diesel::delete(guild_channel_config::table)
            .filter(guild_channel_config::guild_id.eq(TextU64(guild_id)))
            .filter(guild_channel_config::command_name.eq(command_name))
            .filter(guild_channel_config::channel_id.eq_any(channels.iter().map(|&x| TextU64(x))))
            .execute(&*self.conn.lock().await)?;

        Ok(())
    }
}