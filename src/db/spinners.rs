use crate::db::schema::spinners;
use diesel::prelude::*;

use crate::db::*;

#[derive(Clone, Debug, Queryable, Insertable, AsChangeset, Identifiable)]
#[primary_key(discord_id, guild_id)]
#[table_name = "spinners"]
pub struct DbSpinner {
    pub discord_id: TextU64,
    pub channel_id: TextU64,
    pub knocked_user_id: TextU64,
    pub duration: i64,
    pub guild_id: TextU64,
}

impl Db {
    pub async fn add_spinner(&self, spinner: DbSpinner) -> QueryResult<()> {
        diesel::insert_into(spinners::table).values(&spinner).execute(&*self.conn.lock().await)?;

        Ok(())
    }

    pub async fn get_spinner(&self, discord_id: u64, guild_id: u64) -> QueryResult<Option<DbSpinner>> {
        spinners::table.find((discord_id.to_string(), guild_id.to_string()))
            .first(&*self.conn.lock().await).optional()
    }

    pub async fn select_top_10_spinners(&self, guild_id: u64) -> QueryResult<Vec<DbSpinner>> {
        spinners::table.filter(spinners::guild_id.eq(TextU64(guild_id)))
            .limit(10).order(spinners::duration.desc()).load(&*self.conn.lock().await)
    }
}