use crate::db::schema::guilds;
use diesel::prelude::*;

use crate::db::*;

#[derive(Clone, Debug, Queryable, Insertable, AsChangeset, Identifiable)]
#[primary_key(guild_id)]
#[table_name = "guilds"]
pub struct DbGuild {
    pub guild_id: TextU64,
    pub richest_role: TextU64,
    pub longest_spin_role: TextU64,
}

impl DbGuild {
    pub fn default(guild_id: u64) -> Self {
        DbGuild {
            guild_id: TextU64(guild_id),
            richest_role: TextU64(0),
            longest_spin_role: TextU64(0),
        }
    }
}

impl Db {
    pub async fn get_guild(&self, guild_id: u64) -> QueryResult<Option<DbGuild>> {
        guilds::table.find(guild_id.to_string())
            .first(&*self.conn.lock().await).optional()
    }

    pub async fn add_guild(&self, guild: DbGuild) -> QueryResult<()> {
        diesel::insert_into(guilds::table).values(&guild).execute(&*self.conn.lock().await)?;

        Ok(())
    }
}