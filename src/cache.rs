use serenity::client::bridge::gateway::ShardManager;
use serenity::prelude::*;

use std::sync::Arc;
use std::collections::HashMap;
use std::cell::Cell;

use chrono::prelude::*;

use crate::db::Db;

pub struct DbContainer;

impl TypeMapKey for DbContainer {
    type Value = Arc<Db>;
}

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

pub struct BotCacheContainer;

impl TypeMapKey for BotCacheContainer {
    type Value = Arc<Mutex<BotCache>>;
}

pub struct NuggetDrop {
    pub time_start: Cell<DateTime<Utc>>,
    pub time_last_message: Cell<DateTime<Utc>>,
    pub messages: Cell<u32>,
}

pub struct BotCache {
    pub deathmatch: Vec<u64>,
    pub robbing: Vec<u64>,
    pub spinning: Vec<u64>,
    pub nugget_drop: HashMap<u64, NuggetDrop>,
    pub loot_dropping: bool,
    pub shutting_down: bool,
    pub richest: HashMap<u64, u64>,
    pub longest_spin: HashMap<u64, u64>,
}

impl BotCache {
    pub fn new() -> Self {
        BotCache {
            deathmatch: Vec::new(),
            robbing: Vec::new(),
            spinning: Vec::new(),
            nugget_drop: HashMap::new(),
            loot_dropping: false,
            shutting_down: false,
            richest: HashMap::new(),
            longest_spin: HashMap::new(),
        }
    }
}

pub struct BotInfoContainer;

impl TypeMapKey for BotInfoContainer {
    type Value = Arc<Mutex<BotInfo>>;
}

pub struct BotInfo {
    pub command_list: HashMap<String, Vec<String>>,
}

impl BotInfo {
    pub fn new(command_list: HashMap<String, Vec<String>>) -> Self {
        BotInfo {
            command_list,
        }
    }
}