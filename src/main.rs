#![type_length_limit="1161084"] // For ubuntu server ¯\_(ツ)_/¯

#[macro_use]
extern crate diesel;

use nebbot_utils::*;

mod cache;
use cache::*;

mod db;
use db::Db;

mod events;
mod cards;
mod commands;

use serenity:: {
    framework::standard::{
        StandardFramework,
    },
};

use serenity::prelude::*;

use std::env;
use std::sync::Arc;
use std::collections::HashMap;

macro_rules! create_framework {
    ($framework:ident, $commands:ident, $($group:expr),*) => {
        let $framework = StandardFramework::new()
        .configure(|c| c.prefix("!"))
        .help(&crate::commands::HELP)

        $(
            .group($group)
        )*

        .after(commands::after)
        .before(commands::before)
        .normal_message(events::on_message);

        let mut $commands = HashMap::new();

        $commands.insert("other".to_owned(), vec![]);
        $commands.insert("all".to_owned(), vec![]);

        $(
            for c in $group.options.commands {
                $commands.insert(c.options.names[0].to_owned().clone(),
                    c.options.names[1..].iter().map(|&x| String::from(x)).collect());
            }
        )*
    };
}

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();

    let db = Db::new().await;

    let bot_cache = Mutex::new(BotCache::new());

    let token = env::var("DISCORD_TOKEN").expect("Token required in environmental variable DISCORD_TOKEN.");

    create_framework![framework, commands,
        &crate::commands::GENERAL_GROUP,
        &crate::commands::ECONOMY_GROUP,
        &crate::commands::GAMBLING_GROUP,
        &crate::commands::CONFIG_GROUP
    ];

    commands.insert("drop".to_owned(), vec![]);

    let mut client = Client::new(&token)
        .event_handler(crate::events::Handler)
        .framework(framework).await
        .expect("Error creating client");

    info!("Created client!");

    let bot_info = Mutex::new(BotInfo::new(commands));

    info!("Listening for events!");

    let bot_cache_arc = Arc::new(bot_cache);

    {
        let mut data = client.data.write().await;

        data.insert::<DbContainer>(Arc::new(db));
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
        data.insert::<BotCacheContainer>(bot_cache_arc.clone());
        data.insert::<BotInfoContainer>(Arc::new(bot_info));
    }

    if let Err(why) = client.start().await {
        error!("{:?}", why);
    }
}
