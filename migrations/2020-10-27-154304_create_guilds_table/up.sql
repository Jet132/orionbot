-- Your SQL goes here
CREATE TABLE guilds (
  guild_id text PRIMARY KEY NOT NULL,
  richest_role text NOT NULL,
  longest_spin_role text NOT NULL
);