-- Your SQL goes here
CREATE TABLE users (
  discord_id text NOT NULL,
  nuggets bigint NOT NULL,
  last_mine timestamptz NOT NULL,
  last_hourly timestamptz NOT NULL,
  has_pickaxe boolean NOT NULL,
  last_spin timestamptz NOT NULL,
  arrested boolean NOT NULL,
  last_bankrob timestamptz NOT NULL,
  guild_id text NOT NULL,
  PRIMARY KEY(discord_id, guild_id)
);